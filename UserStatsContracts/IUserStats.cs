﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserStatsContracts
{
    public interface IUserStats
    {
        string GetNick();
        int GetTotalGames();
        int GetWonGames();
        int GetLostGames();
        float GetWinToLostRatio();
        int GetTieGames();
    }
}
