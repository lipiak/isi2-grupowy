﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnectionContracts
{
    public delegate void MessageReceivedHandler(object sender, string msg);
    public delegate void PlayerConnectedHandler(object sender, string nick);
    
    //costam
    public interface IConnection
    {
        void StartServer(int port);
        void ConnectToGame(string ip, string nick);
        void Send(string msg);
        void CloseConnections();
        event MessageReceivedHandler onMessageReceived;
        event PlayerConnectedHandler onPlayerConnected;
    }
}
