﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConnectionContracts;
using LogicContracts;
using DatabaseContracts;
using GameContracts;
using System.Web;

namespace Grupowy
{
    public partial class GameList : Form
    {
        Form1 parent;
        IConnection conn;
        ILogic logic;
        IDatabase db;
        string playerName;
        public GameList(Form1 parent, IDatabase db, IConnection connection, ILogic logic, string playerName)
        {
            InitializeComponent();
            this.conn = connection;
            this.logic = logic;
            this.playerName = playerName;
            this.db = db;
            this.parent = parent;
            tbIP.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int port;
            int.TryParse(tbPort.Text, out port);
            conn.StartServer(port);

            //wystawienie ip do bazy
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] a = response.Split(':');
            string a2 = a[1].Substring(1);
            string[] a3 = a2.Split('<');
            string wynikowy = a3[0];

            if (checkBox1.Checked)
                wynikowy = tbIP.Text;

            db.ShowGameInDatabase(wynikowy, tbPort.Text, playerName);
            
            
            plansza p = new plansza(this, conn, logic, db, playerName);
            conn.onMessageReceived += p.MultiplayerMessageReceived;
            conn.onPlayerConnected += p.MultiplayerPlayerConnected;
            p.Show();
            this.Hide();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2) //czyli jesli wcisnieto przycisk Połącz
            {
                string adres = (string)dataGridView1.Rows[e.RowIndex].Cells["Address"].Value;
                string nick = (string)dataGridView1.Rows[e.RowIndex].Cells["Nick"].Value;

                try
                {
                    plansza p = new plansza(this, conn, logic, db, playerName); 
                    conn.onMessageReceived += p.MultiplayerMessageReceived;
                    conn.ConnectToGame(adres, playerName);
                  
                    //jesli polaczony to skasuj z bazy ta gre
                    db.DeleteGameFromDatabase(nick);

                    p.pnlZasłona.Hide();
                    p.Show();
                    this.Hide();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void GameList_FormClosing(object sender, FormClosingEventArgs e)
        {
            parent.Show();
        }

        private void GameList_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                //wypelnij liste z bazy
                dataGridView1.Rows.Clear();
                IList<IGame> gry = db.GetActiveGames();
                foreach (IGame g in gry)
                {
                    dataGridView1.Rows.Add(g.GetOwner(), g.GetIP(), "Połącz");
                }
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
                tbIP.Enabled = true;
            else
                tbIP.Enabled = false;
        }
    }
}
