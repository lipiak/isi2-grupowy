﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Registration
{
    public partial class RegisterForm : Form
    {
        Registration reg;
        public RegisterForm(Registration reg)
        {
            this.reg = reg;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (sprawdzanieTextBox(textBox1) && sprawdzanieTextBox(textBox2) && sprawdzanieTextBox(textBox3) &&
                poprawnoscHasla(textBox2, textBox3))
            {
                reg.GetDB().RegisterUser(tbImie.Text, tbNazwisko.Text, textBox1.Text, textBox2.Text);
                reg.ZakonczRejestracje();
            }
            else if (poprawnoscHasla(textBox2, textBox3) != true)
                MessageBox.Show("Hasła są rózne");
            else
            {
                string tmp = "";
                if (sprawdzanieTextBox(textBox1))
                    tmp += "loginu";
                else if (sprawdzanieTextBox(textBox2))
                    tmp += " hasła";
                else if (sprawdzanieTextBox(textBox3))
                    tmp += " powtórzonego hasła";
                MessageBox.Show("Nie podałeś " + tmp);
            }
        }

        private bool sprawdzanieTextBox(TextBox textBox)
        {
            string zawartosc = textBox.Text;
            if (zawartosc.Length > 0)
                return true;
            else
                return false;
        }

        private bool poprawnoscHasla(TextBox haslo1, TextBox haslo2)
        {
            return haslo1.Text == haslo2.Text ? true : false;
        }


    }
}
