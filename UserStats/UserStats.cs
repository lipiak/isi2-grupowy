﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserStatsContracts;

namespace UserStats
{
    public class UserStats : IUserStats
    {
        string nick;
        int totalGames;
        int wonGames;
        int lostGames;
        int tieGames;

        public UserStats(string nick, int totalGames, int wonGames, int lostGames, int tieGames)
        {
            this.nick = nick;
            this.totalGames = totalGames;
            this.wonGames = wonGames;
            this.lostGames = lostGames;
            this.tieGames = tieGames;
        }

        public string GetNick()
        {
            return this.nick;
        }

        public int GetTotalGames()
        {
            return this.totalGames;
        }

        public int GetWonGames()
        {
            return this.wonGames;
        }

        public int GetLostGames()
        {
            return this.lostGames;
        }

        public float GetWinToLostRatio()
        {
            if (this.lostGames == 0)
                return this.wonGames;
            else
                return (float)this.wonGames / (float)this.lostGames;
        }


        public int GetTieGames()
        {
            return this.tieGames;
        }
    }
}
