﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicContracts;
using ConnectionContracts;
using DatabaseContracts;

namespace Grupowy
{
    public partial class plansza : Form
    {
        ILogic logic;
        IConnection con;
        IDatabase db;
        string symbolPC;
        string symbolGracza;
        string[,] tablica_plansza;
        bool multiplayer;
        bool moj_ruch;
        GameList mp_parent;
        Form1 parent;
        string playerName;

        public plansza(Form1 parent, ILogic logic)
        {
            InitializeComponent();
            this.multiplayer = false;
            this.logic = logic;
            this.pnlZasłona.Hide();
            this.parent = parent;

            //inicjalizuj guziki
            for (int i = 1; i <= 9; i++)
            {
                Button btn = (Button)this.Controls.Find(string.Format("button{0}", i), false)[0];
                btn.Text = "";
                btn.Enabled = true;
                btn.Click += this.OnButtonClick;
            }
        }

        public plansza(GameList mp_parent, IConnection connection, ILogic logic, IDatabase db, string playerName)
        {
            InitializeComponent();
            this.multiplayer = true;
            this.con = connection;
            this.logic = logic;
            this.db = db;
            this.pnlZasłona.Show();
            symbolPC = null;
            symbolGracza = null;
            button11.Enabled = false;
            this.mp_parent = mp_parent;
            this.playerName = playerName;

            //inicjalizuj guziki
            for (int i = 1; i <= 9; i++)
            {
                Button btn = (Button)this.Controls.Find(string.Format("button{0}", i), false)[0];
                btn.Text = "";
                btn.Enabled = true;
                btn.Click += this.OnButtonClick;
            }
        }

        protected void OnButtonClick(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            int mnoznik = 0;
            int koncowka = 0;

            if (moj_ruch || !multiplayer)
            {
                //wstaw symbol na guzik
                btn.Text = symbolGracza;
                btn.Enabled = false;
                //btn.Click -= this.OnButtonClick;
                if(!multiplayer)
                    button11.Enabled = true; //reset button

                string btn_name = btn.Name;
                int btn_index;
                int.TryParse(btn_name.Substring(6, 1), out btn_index);

                //zejsc z zakresu 1-9 do 0-8
                btn_index -= 1;

                //teraz trzeba obliczyc wspolrzedne tego buttona w tablicy
                mnoznik = btn_index / 3;
                koncowka = btn_index - mnoznik * 3;

                //dopisz symbol do tablicy planszy
                tablica_plansza[mnoznik, koncowka] = symbolGracza;
            }

            if (multiplayer && moj_ruch)
            {
                con.Send(string.Format("W {0} K {1}", mnoznik, koncowka));
                moj_ruch = false;

                if (lblCzyjRuch.InvokeRequired)
                    lblCzyjRuch.Invoke(new MethodInvoker(() => { lblCzyjRuch.Text = "Ruch przeciwnika"; }));
                else
                    lblCzyjRuch.Text = "Ruch przeciwnika";
            }

            string ktowygral;
            if (!multiplayer)
            {
                //sprawdz czy nie wygral gracz
                ktowygral = KtoWygral();
                if (ktowygral != null)
                {
                    for (int i = 1; i <= 9; i++)
                    {
                        Button b = (Button)this.Controls.Find(string.Format("button{0}", i), false)[0];
                        b.Enabled = false;
                        //b.Click -= this.OnButtonClick;
                    }

                    if (ktowygral.Equals(symbolPC))
                    {
                        MessageBox.Show("Niestety przegrałeś.");
                    }

                    else if (ktowygral.Equals(symbolGracza))
                    {
                        MessageBox.Show("Brawo! Wygrałeś!");
                    }

                    else if (ktowygral.Equals("remis"))
                    {
                        MessageBox.Show("Remis!");
                    }

                    button10.Enabled = true;
                    return;
                }
            }


            if (!multiplayer)
            {
                //a teraz ruch przeciwnika
                string[,] nowaPlansza = logic.PCMove(tablica_plansza, symbolPC);
                AktualizujPlansze(nowaPlansza);
            }
            //a moze teraz ktos wygral
            ktowygral = KtoWygral();
            if (ktowygral != null)
            {
                for (int i = 1; i <= 9; i++)
                {
                    Button b = (Button)this.Controls.Find(string.Format("button{0}", i), false)[0];
                    b.Enabled = false;
                    //b.Click -= this.OnButtonClick;
                }

                if (ktowygral.Equals(symbolPC))
                {
                    if (multiplayer)
                    {
                        db.UpdateStatsUserLost(playerName);                        
                    }
                    MessageBox.Show("Niestety przegrałeś.");
                }

                else if (ktowygral.Equals(symbolGracza))
                {
                    if (multiplayer)
                        db.UpdateStatsUserWin(playerName);
                    MessageBox.Show("Brawo! Wygrałeś!");
                }

                else if (ktowygral.Equals("remis"))
                {
                    if (multiplayer)
                        db.UpdateStatsTie(playerName);
                    MessageBox.Show("Remis!");
                }

                button10.Enabled = true;
                return;
            }
            
        }
        

        private void PrzygotujGuzikiPodMultiplayer()
        {
            for (int i = 1; i <= 9; i++)
            {
                Button btn = (Button)this.Controls.Find(string.Format("button{0}", i), false)[0];

                if (btn.InvokeRequired)
                    btn.Invoke(new MethodInvoker(() =>
                        {
                            btn.Text = "";
                            btn.Enabled = true;
                            //btn.Click += this.OnButtonClick;
                        }));
                else
                {
                    btn.Text = "";
                    btn.Enabled = true;
                    //btn.Click += this.OnButtonClick;
                }
            }
        }

        //rozpoczecie nowej gry
        private void button10_Click(object sender, EventArgs e)
        {
            button11.Enabled = false;
            //inicjalizuj guziki
            if (!multiplayer)
            {
                for (int i = 1; i <= 9; i++)
                {
                    Button btn = (Button)this.Controls.Find(string.Format("button{0}", i), false)[0];
                    btn.Text = "";
                    btn.Enabled = true;
                    //btn.Click += this.OnButtonClick;
                }
            }
            else PrzygotujGuzikiPodMultiplayer();


            Button wywolany = (Button)sender;
            wywolany.Enabled = false;

            if(multiplayer)
                MultiplayerPlayerConnected(null, null);

            if (!multiplayer)
            {
                //losuj symbol pc
                var r = new Random();
                if (r.Next(2000) % 2 == 0)
                {
                    symbolPC = "O";
                    symbolGracza = "X";
                }
                else
                {
                    symbolPC = "X";
                    symbolGracza = "O";
                }



                //inicjalizacja tablicy stringow i wypelnienie nullami
                tablica_plansza = new string[3, 3];

                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        tablica_plansza[i, j] = null;
                    }
                }

                MessageBox.Show(string.Format("Symbol komputera to: {0}", symbolPC));

                //postaw symbol na pole, jesli zaczyna komputer
                if (symbolPC.Equals("O"))
                {
                    string[,] nowaPlansza = logic.PCMove(tablica_plansza, symbolPC);
                    AktualizujPlansze(nowaPlansza);
                    button11.Enabled = true;
                }
            }
        }

        //tutaj zerowanie gry, bez losowania symbolu komputera
        private void button11_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            b.Enabled = false;

            //inicjalizacja tablicy stringow i wypelnienie nullami
            tablica_plansza = new string[3, 3];

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    tablica_plansza[i, j] = null;
                }
            }

            //inicjalizuj guziki
            for (int i = 1; i <= 9; i++)
            {
                Button btn = (Button)this.Controls.Find(string.Format("button{0}", i), false)[0];
                btn.Text = "";
                btn.Enabled = true;
                //btn.Click += this.OnButtonClick;
            }

            //postaw symbol na pole, jesli zaczyna komputer
            if (symbolPC.Equals("O"))
            {
                string[,] nowaPlansza = logic.PCMove(tablica_plansza, symbolPC);
                
                AktualizujPlansze(nowaPlansza);
                b.Enabled = true;
            }
        }

        private void AktualizujPlansze(string[,] nowaPlansza)
        {
            //przeszukaj plansze i wstaw odpowienie wartosci na guziki
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (nowaPlansza[i, j] == null) continue;
                    int nr_buttona = i * 3 + j + 1;
                    Button btn = (Button)Controls.Find(string.Format("button{0}", nr_buttona), false)[0];
                    btn.Text = nowaPlansza[i, j];
                    btn.Enabled = false;
                    //btn.Click -= this.OnButtonClick;
                }
            }
            tablica_plansza = nowaPlansza;
        }

        private string KtoWygral()
        {
            return logic.CheckWinner(tablica_plansza);
        }

        //a to się będzie wykonywać u każdego
        public void MultiplayerMessageReceived(object sender, string msg)
        {
            if (msg.StartsWith("TWOJ_SYMBOL"))
            {
                //wyzeruj tablice, bo jak widac nowa gra sie rozpoczela
                tablica_plansza = new string[3, 3];
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        tablica_plansza[i, j] = null;
                    }
                }

                PrzygotujGuzikiPodMultiplayer();

                string[] split = msg.Split(' ');
                symbolGracza = split[1];

                if (symbolGracza.Equals("X"))
                {
                    symbolPC = "O";
                    moj_ruch = false;

                    if (lblCzyjRuch.InvokeRequired)
                        lblCzyjRuch.Invoke(new MethodInvoker(() => { lblCzyjRuch.Text = "Ruch przeciwnika"; }));
                    else
                        lblCzyjRuch.Text = "Ruch przeciwnika";
                }
                else
                {
                    symbolPC = "X";
                    moj_ruch = true;

                    if (lblCzyjRuch.InvokeRequired)
                        lblCzyjRuch.Invoke(new MethodInvoker(() => { lblCzyjRuch.Text = "Twoj ruch"; }));
                    else
                        lblCzyjRuch.Text = "Twoj ruch";
                }

                //jeszcze wylaczyc guzik nowej gry
                if (button10.InvokeRequired)
                    button10.Invoke(new MethodInvoker(() => { button10.Enabled = false; }));
                else
                    button10.Enabled = false;
            }

            //a moze otrzymano pole na ktore trzeba wstawic figure
            //format to W x K y gdzie x i y to numer wiersza/kolumny w ktory wstawic symbol
            string[] split2 = msg.Split(' ');
            if (split2.Length == 4 && split2[0].Equals("W") && split2[2].Equals("K"))
            {
                int wiersz, kolumna;
                int.TryParse(split2[1], out wiersz);
                int.TryParse(split2[3], out kolumna);

                //obliczyć na podstawie tego numer buttona, do którego trzeba się dostać
                int btn_index = wiersz * 3 + kolumna + 1;

                //znalezc button
                Button btn = (Button)Controls.Find(string.Format("button{0}", btn_index), false)[0];

                //jesli wywołanie międzywątkowe
                if (btn.InvokeRequired)
                {
                    btn.Invoke(new MethodInvoker(() =>
                        {
                            btn.Text = symbolPC;
                            btn.Enabled = false;
                            //btn.Click -= this.OnButtonClick;
                        }));
                }
                else
                {
                    btn.Text = symbolPC;
                    btn.Enabled = false;
                    //btn.Click -= this.OnButtonClick;
                }

                //na guziku już jest, teraz dostawić do tablicy
                tablica_plansza[wiersz, kolumna] = symbolPC;

                //sprawdzic czy ktos przypadkiem nie wygral
                string ktowygral = KtoWygral();
                if (ktowygral != null)
                {
                    for (int i = 1; i <= 9; i++)
                    {
                        Button b = (Button)this.Controls.Find(string.Format("button{0}", i), false)[0];

                        if (b.InvokeRequired)
                            b.Invoke(new MethodInvoker(() =>
                            {
                                b.Text = "";
                                b.Enabled = true;
                                //b.Click -= this.OnButtonClick;
                            }));
                        else
                        {
                            b.Text = "";
                            b.Enabled = true;
                            //b.Click -= this.OnButtonClick;
                        }
                    }

                    if (ktowygral.Equals(symbolPC))
                    {
                        db.UpdateStatsUserLost(playerName);
                        MessageBox.Show("Niestety przegrałeś.");
                    }

                    else if (ktowygral.Equals(symbolGracza))
                    {
                        db.UpdateStatsUserWin(playerName);
                        MessageBox.Show("Brawo! Wygrałeś!");
                    }

                    else if (ktowygral.Equals("remis"))
                    {
                        db.UpdateStatsTie(playerName);
                        MessageBox.Show("Remis!");
                    }


                    //jeszcze wylaczyc guzik nowej gry
                    if (button10.InvokeRequired)
                        button10.Invoke(new MethodInvoker(() => { button10.Enabled = true; }));
                    else
                        button10.Enabled = true;
                    
                    return;
                }

                //to byl ruch przeciwnika, a teraz moj
                moj_ruch = true;
                if (lblCzyjRuch.InvokeRequired)
                    lblCzyjRuch.Invoke(new MethodInvoker(() => { lblCzyjRuch.Text = "Twoj ruch"; }));
                else
                    lblCzyjRuch.Text = "Twoj ruch";
            }
        }

        //ta metoda się wykona tylko jeśli użytkownik jest serwerem
        public void MultiplayerPlayerConnected(object sender, string msg)
        {
            //jesli taka wiadomosc otrzymana, to znaczy ze jestem serwerem
            //wyzeruj tablice gry
            tablica_plansza = new string[3, 3];
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    tablica_plansza[i, j] = null;
                }
            }

            PrzygotujGuzikiPodMultiplayer();


            //wylosuj symbole
            string[] symbole = { "O", "X" };
            int symbol_id = (new Random().Next(2000)) % 2;
            symbolGracza = symbole[symbol_id];
            symbolPC = symbole[1 - symbol_id];
            con.Send(string.Format("TWOJ_SYMBOL {0}", symbolPC));

            if (symbolGracza.Equals("O"))
            {
                moj_ruch = true;

                if (lblCzyjRuch.InvokeRequired)
                    lblCzyjRuch.Invoke(new MethodInvoker(() => { lblCzyjRuch.Text = "Twoj ruch"; }));
                else
                    lblCzyjRuch.Text = "Twoj ruch";
            }
            else
            {
                moj_ruch = false;

                if (lblCzyjRuch.InvokeRequired)
                    lblCzyjRuch.Invoke(new MethodInvoker(() => { lblCzyjRuch.Text = "Ruch przeciwnika"; }));
                else
                    lblCzyjRuch.Text = "Ruch przeciwnika";
            }

            if (pnlZasłona.InvokeRequired)
                pnlZasłona.Invoke(new MethodInvoker(() => { pnlZasłona.Hide(); }));
            else 
                pnlZasłona.Hide();

            //jeszcze wylaczyc guzik nowej gry
            if (button10.InvokeRequired)
                button10.Invoke(new MethodInvoker(() => { button10.Enabled = false; }));
            else
                button10.Enabled = false;
        }

        private void plansza_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(multiplayer)
                db.DeleteGameFromDatabase(playerName);

            if (con != null)
                con.CloseConnections();

            if (mp_parent != null)
                mp_parent.Show();

            if (parent != null)
                parent.Show();
        }       
    }
}
