﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameContracts;
using UserStatsContracts;

namespace DatabaseContracts
{
    public interface IDatabase
    {
        bool RegisterUser(string imie, string nazwisko, string login, string haslo);
        bool LoginUser(string login, string haslo);
        IList<IGame> GetActiveGames();
        IList<IUserStats> GetStatistics();
        void UpdateStatsUserWin(string nick);
        void UpdateStatsUserLost(string nick);
        void UpdateStatsTie(string nick);
        void ShowGameInDatabase(string ip, string port, string nick);
        void DeleteGameFromDatabase(string nick);
    }
}
