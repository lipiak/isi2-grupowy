﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DatabaseContracts;
using GameContracts;
using Game;
using UserStatsContracts;
using UserStats;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using g = Game;

namespace Database
{
    /// <summary>
    /// Na starcie trzeba uzyć funkcji Init
    /// </summary>
    public class Database : IDatabase
    {
        private MySqlConnection connection;
        private bool czyPolaczony;

        public Database()
        {
            //inicjalizacja połączenia z bazą
            string connectionString = "server=db4free.net;Port=3306; User ID=bartekwojcik; password=plokij; database=grypk";
            connection = new MySqlConnection(connectionString);
            czyPolaczony = DatabaseHelper.OpenConnection(connection);
        }

        public void Init()
        {
            //inicjalizacja połączenia z bazą
            string connectionString = "server=db4free.net;Port=3306; User ID=bartekwojcik; password=plokij; database=grypk";
            connection = new MySqlConnection(connectionString);
            czyPolaczony = DatabaseHelper.OpenConnection(connection);
        }
        /// <summary>
        /// zwraca true jesli rejestracja sie powiodła, false w innym przypadku
        /// </summary>
        /// <param name="imie"></param>
        /// <param name="nazwisko"></param>
        /// <param name="login"></param>
        /// <param name="haslo"></param>
        /// <returns></returns>
        public bool RegisterUser(string imie, string nazwisko, string login, string haslo)
        {
            //sprawdzenie czy istnieje juz taki typek
            string query = string.Format("select login from uzytkownicy where login ='{0}'", login);

            if (czyPolaczony)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataReader datareader = cmd.ExecuteReader();

                while (datareader.Read())
                {
                    string nick = datareader["login"].ToString();
                    if (nick == login)
                    {
                        MessageBox.Show("uzytkownik o podanym loginie juz istnieje");
                        return false;
                    }
                }
                datareader.Close();
            }
            //jesli tu jestesmy to nie istnieje i go wsadzamy
            query = string.Format("insert into uzytkownicy (login,haslo,imie,nazwisko) values ('{0}','{1}','{2}','{3}')", login, haslo, imie, nazwisko);
            if (czyPolaczony)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                cmd.ExecuteNonQuery();

            }
            //umieszkamy tego typka w rankingu z wynikiem 0
            query = string.Format("insert into ranking (osoba,totalGames,wonGames,lostGames) values ('{0}','0','0','0')", login);
            if (czyPolaczony)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                cmd.ExecuteNonQuery();

            }
            return true;
        }
        /// <summary>
        /// sprawdza, czy dany user istnieje jesl tak to czy podane hasło się zgadza, nie za bardzo wybrazam sobie jak inaczej by miało to działać.
        /// w ogóle głupia funkcja. powinna nazywać się jakoś "CheckLoginPasswordPair" lub jakoś w ten deseń
        /// </summary>
        /// <param name="login"></param>
        /// <param name="haslo"></param>
        /// <returns></returns>
        public bool LoginUser(string login, string haslo)
        {
            if (czyPolaczony)
            {
                string query = string.Format("select login, haslo from uzytkownicy where login='{0}' and haslo = '{1}'", login, haslo);
                MySqlCommand cmd = new MySqlCommand(query, connection);

                MySqlDataReader dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    var templogin = dataReader["login"].ToString();
                    var temphaslo = dataReader["haslo"].ToString();

                    if (login == templogin && haslo == temphaslo)
                    {
                        dataReader.Close();
                        return true;
                    }
                }
                dataReader.Close();
            }
            return false;
        }

        /// <summary>
        /// kon jaki jest kazdy widzi
        /// </summary>
        /// <returns></returns>
        public IList<IGame> GetActiveGames()
        {
            List<IGame> listagier = new List<IGame>();
            if (czyPolaczony)
            {

                string query = "select * from aktywne_gry";
                MySqlCommand cmd = new MySqlCommand(query, connection);

                MySqlDataReader dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    var osoba = dataReader["osoba"].ToString();
                    var zewnetrzneIp = dataReader["zewnetrzne_ip"].ToString();
                    if (!string.IsNullOrEmpty(osoba) && !string.IsNullOrEmpty(zewnetrzneIp))
                    {
                        var game = new g.Game(zewnetrzneIp, osoba);
                        listagier.Add(game);
                    }
                }
                dataReader.Close();
            }
            return listagier;

        }

        /// <summary>
        /// kon jaki jest kazdy widzi
        /// </summary>
        /// <returns></returns>
        public IList<UserStatsContracts.IUserStats> GetStatistics()
        {
            List<UserStatsContracts.IUserStats> lista = new List<IUserStats>();
            if (czyPolaczony)
            {
                 string query = "select * from ranking";
                MySqlCommand cmd = new MySqlCommand(query, connection);
                
                MySqlDataReader dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    var osoba = dataReader["osoba"].ToString();
                    var wongamesString = dataReader["wonGames"].ToString();
                    var lostGamesString = dataReader["lostGames"].ToString();
                    var totalGamesString = dataReader["totalGames"].ToString();
                    var tieGamesString = dataReader["tieGames"].ToString();

                    int wongamesINT;
                    var wongamesBool = Int32.TryParse(wongamesString,out wongamesINT);

                    int lostGamesINT;
                    var lostGamesBool = Int32.TryParse(lostGamesString, out lostGamesINT);

                    int totalGamesINT;
                    var totalGamesBool = Int32.TryParse(totalGamesString,out totalGamesINT);

                    int tiegamesINT;
                    var tiegamesBool = Int32.TryParse(tieGamesString, out tiegamesINT);

                    if (tiegamesBool && wongamesBool && lostGamesBool && totalGamesBool)
                    {
                        IUserStats stats = new UserStats.UserStats(osoba, totalGamesINT, wongamesINT, lostGamesINT, tiegamesINT);
                        lista.Add(stats);
                    }
                }
                dataReader.Close();
            }
            return lista;
        }

        /// <summary>
        /// dodaje +1 do puli zwycięstw i total gier gracza
        /// </summary>
        /// <param name="nick"></param>
        public void UpdateStatsUserWin(string nick)
        {
            int wygrane = 0;
            int total = 0;
            int remisy = 0;
            if (czyPolaczony)
            {
                string query = string.Format("select * from ranking where osoba = '{0}'",nick);
                MySqlCommand cmd = new MySqlCommand(query, connection);

                MySqlDataReader dataReader = cmd.ExecuteReader();
               
                while (dataReader.Read())
                {
                    var osoba = dataReader["osoba"].ToString();
                    var wongamesString = dataReader["wonGames"].ToString();
                    var totalGamesString = dataReader["totalGames"].ToString();

                    int wongamesINT;
                    var wongamesBool = Int32.TryParse(wongamesString, out wongamesINT);


                    int totalGamesINT;
                    var totalGamesBool = Int32.TryParse(totalGamesString, out totalGamesINT);


                    if (wongamesBool && totalGamesBool && osoba == nick)
                    {
                        wygrane = wongamesINT+1;
                        total = totalGamesINT+1;

                    }
                }
                dataReader.Close();

                query = string.Format("update ranking set totalGames = '{0}', wonGames = '{1}' where osoba = '{2}'",total,wygrane,nick);
                MySqlCommand cmd2 = new MySqlCommand(); // mozna by uzyc wczesniej zdefiniowanego cmd ale boje sie by to się nie jebało jak w javie (gdzie jeden MySQLCommand tworzony jest tylko do jednego query"
                
                cmd2.CommandText = query;
                cmd2.Connection = connection;
                cmd2.ExecuteNonQuery();             
            }
        }
        /// <summary>
        /// odejmuje 1 od puli przegranych gracza i total gier gracza
        /// </summary>
        /// <param name="nick"></param>
        public void UpdateStatsUserLost(string nick)
        {
            int przegrane = 0;
            int total = 0;
            if (czyPolaczony)
            {
                string query = string.Format("select * from ranking where osoba = '{0}'", nick);
                MySqlCommand cmd = new MySqlCommand(query, connection);

                MySqlDataReader dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    var osoba = dataReader["osoba"].ToString();
                    var lostgamesString = dataReader["lostGames"].ToString();
                    var totalGamesString = dataReader["totalGames"].ToString();

                    int lostgamesINT;
                    var lostgamesBool = Int32.TryParse(lostgamesString, out lostgamesINT);


                    int totalGamesINT;
                    var totalGamesBool = Int32.TryParse(totalGamesString, out totalGamesINT);

                    if (lostgamesBool && totalGamesBool && osoba == nick)
                    {
                        przegrane = lostgamesINT + 1;
                        total = totalGamesINT + 1;

                    }
                }

                dataReader.Close();
                query = string.Format("update ranking set totalGames = '{0}', lostGames = '{1}' where osoba = '{2}'", total, przegrane, nick);
                MySqlCommand cmd2 = new MySqlCommand(); // mozna by uzyc wczesniej zdefiniowanego cmd ale boje sie by to się nie jebało jak w javie (gdzie jeden MySQLCommand tworzony jest tylko do jednego query"

                cmd2.CommandText = query;
                cmd2.Connection = connection;
                cmd2.ExecuteNonQuery();
            }
        }

        //brakuje wg mnie nastepujacych metod:
        //SetNewGame (tworzy nowy wpis w tabeli aktywne_gry pobierajac IP i login)


        public void ShowGameInDatabase(string ip, string port, string nick)
        {
            try
            {
                if (czyPolaczony)
                {

                    string query = string.Format("insert into aktywne_gry (osoba, zewnetrzne_ip) values ('{0}','{1}')", nick, ip + ":" + port);
                    MySqlCommand cmd2 = new MySqlCommand();
                    cmd2.CommandText = query;
                    cmd2.Connection = connection;
                    cmd2.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        public void DeleteGameFromDatabase(string nick)
        {
            try
            {
                if (czyPolaczony)
                {
                    string query = string.Format("delete from aktywne_gry where osoba = '{0}'", nick);
                    MySqlCommand cmd2 = new MySqlCommand();
                    cmd2.CommandText = query;
                    cmd2.Connection = connection;
                    cmd2.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        //tylko podbija liczbe wszystkich gier o 1
        public void UpdateStatsTie(string nick)
        {
            int remisy = 0;
            int total = 0;
            if (czyPolaczony)
            {
                string query = string.Format("select * from ranking where osoba = '{0}'", nick);
                MySqlCommand cmd = new MySqlCommand(query, connection);

                MySqlDataReader dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    var osoba = dataReader["osoba"].ToString();
                    var tieGamesString = dataReader["tieGames"].ToString();
                    var totalGamesString = dataReader["totalGames"].ToString();

                    int tieGamesINT;
                    var tieGamesBool = Int32.TryParse(tieGamesString, out tieGamesINT);


                    int totalGamesINT;
                    var totalGamesBool = Int32.TryParse(totalGamesString, out totalGamesINT);

                    if (tieGamesBool && totalGamesBool && osoba == nick)
                    {
                        remisy = tieGamesINT + 1;
                        total = totalGamesINT + 1;

                    }
                }
                dataReader.Close();

                query = string.Format("update ranking set totalGames = '{0}', tieGames = '{1}' where osoba = '{2}'", total, remisy, nick);
                MySqlCommand cmd2 = new MySqlCommand(); // mozna by uzyc wczesniej zdefiniowanego cmd ale boje sie by to się nie jebało jak w javie (gdzie jeden MySQLCommand tworzony jest tylko do jednego query"

                cmd2.CommandText = query;
                cmd2.Connection = connection;
                cmd2.ExecuteNonQuery();
            }
        }
    }
}
