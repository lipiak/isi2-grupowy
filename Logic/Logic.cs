﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogicContracts;

namespace Logic
{
    public class Logic : ILogic  
    {
        public string[,] PCMove(string[,] aktualnaPlansza, string symbolKomputera)
        {
            //wiesz pierwszy
            if (aktualnaPlansza[0, 0] == symbolKomputera && aktualnaPlansza[1, 0] == symbolKomputera)
            {
                if (aktualnaPlansza[2, 0] == null)
                {
                    aktualnaPlansza[2, 0] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[0, 0] == symbolKomputera && aktualnaPlansza[2, 0] == symbolKomputera)
            {
                if (aktualnaPlansza[1, 0] == null)
                {
                    aktualnaPlansza[1, 0] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[1, 0] == symbolKomputera && aktualnaPlansza[2, 0] == symbolKomputera)
            {
                if (aktualnaPlansza[0, 0] == null)
                {
                    aktualnaPlansza[0, 0] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }
            //wiesz drugi
            if (aktualnaPlansza[0, 1] == symbolKomputera && aktualnaPlansza[1, 1] == symbolKomputera)
            {
                if (aktualnaPlansza[2, 1] == null)
                {
                    aktualnaPlansza[2, 1] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[0, 1] == symbolKomputera && aktualnaPlansza[2, 1] == symbolKomputera)
            {
                if (aktualnaPlansza[1, 1] == null)
                {
                    aktualnaPlansza[1, 1] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[1, 1] == symbolKomputera && aktualnaPlansza[2, 1] == symbolKomputera)
            {
                if (aktualnaPlansza[0, 1] == null)
                {
                    aktualnaPlansza[0, 1] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }


            //wiersz trzeci
            if (aktualnaPlansza[0, 2] == symbolKomputera && aktualnaPlansza[1, 2] == symbolKomputera)
            {
                if (aktualnaPlansza[2, 2] == null)
                {
                    aktualnaPlansza[2, 2] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[0, 2] == symbolKomputera && aktualnaPlansza[2, 2] == symbolKomputera)
            {
                if (aktualnaPlansza[1, 2] == null)
                {
                    aktualnaPlansza[1, 2] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[1, 2] == symbolKomputera && aktualnaPlansza[2, 2] == symbolKomputera)
            {
                if (aktualnaPlansza[0, 2] == null)
                {
                    aktualnaPlansza[0, 2] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            //kolumna pierwsza
            if (aktualnaPlansza[0, 0] == symbolKomputera && aktualnaPlansza[0, 1] == symbolKomputera)
            {
                if (aktualnaPlansza[0, 2] == null)
                {
                    aktualnaPlansza[0, 2] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[0, 0] == symbolKomputera && aktualnaPlansza[0, 2] == symbolKomputera)
            {
                if (aktualnaPlansza[0, 1] == null)
                {
                    aktualnaPlansza[0, 1] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[0, 1] == symbolKomputera && aktualnaPlansza[0, 2] == symbolKomputera)
            {
                if (aktualnaPlansza[0, 0] == null)
                {
                    aktualnaPlansza[0, 0] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            //kolumna druga
            if (aktualnaPlansza[1, 0] == symbolKomputera && aktualnaPlansza[1, 1] == symbolKomputera)
            {
                if (aktualnaPlansza[1, 2] == null)
                {
                    aktualnaPlansza[1, 2] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[1, 0] == symbolKomputera && aktualnaPlansza[1, 2] == symbolKomputera)
            {
                if (aktualnaPlansza[1, 1] == null)
                {
                    aktualnaPlansza[1, 1] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[1, 1] == symbolKomputera && aktualnaPlansza[1, 2] == symbolKomputera)
            {
                if (aktualnaPlansza[1, 0] == null)
                {
                    aktualnaPlansza[1, 0] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            //kolumna trzecia
            if (aktualnaPlansza[2, 0] == symbolKomputera && aktualnaPlansza[2, 1] == symbolKomputera)
            {
                if (aktualnaPlansza[2, 2] == null)
                {
                    aktualnaPlansza[2, 2] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[2, 0] == symbolKomputera && aktualnaPlansza[2, 2] == symbolKomputera)
            {
                if (aktualnaPlansza[2, 1] == null)
                {
                    aktualnaPlansza[2, 1] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[2, 1] == symbolKomputera && aktualnaPlansza[2, 2] == symbolKomputera)
            {
                if (aktualnaPlansza[2, 0] == null)
                {
                    aktualnaPlansza[2, 0] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            //skos 1 5 9
            if (aktualnaPlansza[0, 0] == symbolKomputera && aktualnaPlansza[1, 1] == symbolKomputera)
            {
                if (aktualnaPlansza[2, 2] == null)
                {
                    aktualnaPlansza[2, 2] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[0, 0] == symbolKomputera && aktualnaPlansza[2, 2] == symbolKomputera)
            {
                if (aktualnaPlansza[1, 1] == null)
                {
                    aktualnaPlansza[1, 1] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[1, 1] == symbolKomputera && aktualnaPlansza[2, 2] == symbolKomputera)
            {
                if (aktualnaPlansza[0, 0] == null)
                {
                    aktualnaPlansza[0, 0] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            //skos 3 5 7
            if (aktualnaPlansza[2, 0] == symbolKomputera && aktualnaPlansza[1, 1] == symbolKomputera)
            {
                if (aktualnaPlansza[0, 2] == null)
                {
                    aktualnaPlansza[0, 2] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[2, 0] == symbolKomputera && aktualnaPlansza[0, 2] == symbolKomputera)
            {
                if (aktualnaPlansza[1, 1] == null)
                {
                    aktualnaPlansza[1, 1] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[1, 1] == symbolKomputera && aktualnaPlansza[0, 2] == symbolKomputera)
            {
                if (aktualnaPlansza[2, 0] == null)
                {
                    aktualnaPlansza[2, 0] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            //koniec ataku na win

            //zaczynamy sie bronic ;)
            //odpowiednio pierwszy wiersz

            if (aktualnaPlansza[0, 0] != symbolKomputera && aktualnaPlansza[0, 0] != null
                && aktualnaPlansza[1, 0] != symbolKomputera && aktualnaPlansza[1, 0] != null)
            {
                if (aktualnaPlansza[2, 0] == null)
                {
                    aktualnaPlansza[2, 0] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[0, 0] != symbolKomputera && aktualnaPlansza[0, 0] != null
                && aktualnaPlansza[2, 0] != symbolKomputera && aktualnaPlansza[2, 0] != null)
            {
                if (aktualnaPlansza[1, 0] == null)
                {
                    aktualnaPlansza[1, 0] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[1, 0] != symbolKomputera && aktualnaPlansza[1, 0] != null
                && aktualnaPlansza[2, 0] != symbolKomputera && aktualnaPlansza[2, 0] != null)
            {
                if (aktualnaPlansza[0, 0] == null)
                {
                    aktualnaPlansza[0, 0] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            //drugi wiersz
            if (aktualnaPlansza[0, 1] != symbolKomputera && aktualnaPlansza[0, 1] != null
                && aktualnaPlansza[1, 1] != symbolKomputera && aktualnaPlansza[1, 1] != null)
            {
                if (aktualnaPlansza[2, 1] == null)
                {
                    aktualnaPlansza[2, 1] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[0, 1] != symbolKomputera && aktualnaPlansza[0, 1] != null
                && aktualnaPlansza[2, 1] != symbolKomputera && aktualnaPlansza[2, 1] != null)
            {
                if (aktualnaPlansza[1, 1] == null)
                {
                    aktualnaPlansza[1, 1] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[1, 1] != symbolKomputera && aktualnaPlansza[1, 1] != null
                && aktualnaPlansza[2, 1] != symbolKomputera && aktualnaPlansza[2, 1] != null)
            {
                if (aktualnaPlansza[0, 1] == null)
                {
                    aktualnaPlansza[0, 1] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            //wiersz trzeci
            if (aktualnaPlansza[0, 2] != symbolKomputera && aktualnaPlansza[0, 2] != null
                && aktualnaPlansza[1, 2] != symbolKomputera && aktualnaPlansza[1, 2] != null)
            {
                if (aktualnaPlansza[2, 2] == null)
                {
                    aktualnaPlansza[2, 2] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[0, 2] != symbolKomputera && aktualnaPlansza[0, 2] != null
                && aktualnaPlansza[2, 2] != symbolKomputera && aktualnaPlansza[2, 2] != null)
            {
                if (aktualnaPlansza[1, 2] == null)
                {
                    aktualnaPlansza[1, 2] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[1, 2] != symbolKomputera && aktualnaPlansza[1, 2] != null
                && aktualnaPlansza[2, 2] != symbolKomputera && aktualnaPlansza[2, 2] != null)
            {
                if (aktualnaPlansza[0, 2] == null)
                {
                    aktualnaPlansza[0, 2] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            //kolumna pierwsza
            if (aktualnaPlansza[0, 0] != symbolKomputera && aktualnaPlansza[0, 0] != null
                && aktualnaPlansza[0, 1] != symbolKomputera && aktualnaPlansza[0, 1] != null)
            {
                if (aktualnaPlansza[0, 2] == null)
                {
                    aktualnaPlansza[0, 2] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[0, 0] != symbolKomputera && aktualnaPlansza[0, 0] != null
                && aktualnaPlansza[0, 2] != symbolKomputera && aktualnaPlansza[0, 2] != null)
            {
                if (aktualnaPlansza[0, 1] == null)
                {
                    aktualnaPlansza[0, 1] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[0, 1] != symbolKomputera && aktualnaPlansza[0, 1] != null
                && aktualnaPlansza[0, 2] != symbolKomputera && aktualnaPlansza[0, 2] != null)
            {
                if (aktualnaPlansza[0, 0] == null)
                {
                    aktualnaPlansza[0, 0] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            //kolumna druga
            if (aktualnaPlansza[1, 0] != symbolKomputera && aktualnaPlansza[1, 0] != null
                && aktualnaPlansza[1, 1] != symbolKomputera && aktualnaPlansza[1, 1] != null)
            {
                if (aktualnaPlansza[1, 2] == null)
                {
                    aktualnaPlansza[1, 2] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[1, 0] != symbolKomputera && aktualnaPlansza[1, 0] != null
                && aktualnaPlansza[1, 2] != symbolKomputera && aktualnaPlansza[1, 2] != null)
            {
                if (aktualnaPlansza[1, 1] == null)
                {
                    aktualnaPlansza[1, 1] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[1, 1] != symbolKomputera && aktualnaPlansza[1, 1] != null
                && aktualnaPlansza[1, 2] != symbolKomputera && aktualnaPlansza[1, 2] != null)
            {
                if (aktualnaPlansza[1, 0] == null)
                {
                    aktualnaPlansza[1, 0] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            //kolumna trzecia

            if (aktualnaPlansza[2, 0] != symbolKomputera && aktualnaPlansza[2, 0] != null
                && aktualnaPlansza[2, 1] != symbolKomputera && aktualnaPlansza[2, 1] != null)
            {
                if (aktualnaPlansza[2, 2] == null)
                {
                    aktualnaPlansza[2, 2] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[2, 0] != symbolKomputera && aktualnaPlansza[2, 0] != null
                && aktualnaPlansza[2, 2] != symbolKomputera && aktualnaPlansza[2, 2] != null)
            {
                if (aktualnaPlansza[2, 1] == null)
                {
                    aktualnaPlansza[2, 1] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[2, 1] != symbolKomputera && aktualnaPlansza[2, 1] != null
                && aktualnaPlansza[2, 2] != symbolKomputera && aktualnaPlansza[2, 2] != null)
            {
                if (aktualnaPlansza[2, 0] == null)
                {
                    aktualnaPlansza[2, 0] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            //skos 1 5 9
            if (aktualnaPlansza[0, 0] != symbolKomputera && aktualnaPlansza[0, 0] != null
                && aktualnaPlansza[1, 1] != symbolKomputera && aktualnaPlansza[1, 1] != null)
            {
                if (aktualnaPlansza[2, 2] == null)
                {
                    aktualnaPlansza[2, 2] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[0, 0] != symbolKomputera && aktualnaPlansza[0, 0] != null
                && aktualnaPlansza[2, 2] != symbolKomputera && aktualnaPlansza[2, 2] != null)
            {
                if (aktualnaPlansza[1, 1] == null)
                {
                    aktualnaPlansza[1, 1] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[1, 1] != symbolKomputera && aktualnaPlansza[1, 1] != null
                && aktualnaPlansza[2, 2] != symbolKomputera && aktualnaPlansza[2, 2] != null)
            {
                if (aktualnaPlansza[0, 0] == null)
                {
                    aktualnaPlansza[0, 0] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            //skos 3 5 7
            if (aktualnaPlansza[2, 0] != symbolKomputera && aktualnaPlansza[2, 0] != null
                && aktualnaPlansza[1, 1] != symbolKomputera && aktualnaPlansza[1, 1] != null)
            {
                if (aktualnaPlansza[0, 2] == null)
                {
                    aktualnaPlansza[0, 2] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[2, 0] != symbolKomputera && aktualnaPlansza[2, 0] != null
                && aktualnaPlansza[0, 2] != symbolKomputera && aktualnaPlansza[0, 2] != null)
            {
                if (aktualnaPlansza[1, 1] == null)
                {
                    aktualnaPlansza[1, 1] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[1, 1] != symbolKomputera && aktualnaPlansza[1, 1] != null
                && aktualnaPlansza[0, 2] != symbolKomputera && aktualnaPlansza[0, 2] != null)
            {
                if (aktualnaPlansza[2, 0] == null)
                {
                    aktualnaPlansza[2, 0] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            //gdy mamy wolne, stawiamy po srodku
            if (aktualnaPlansza[1, 1] == null)
            {
                aktualnaPlansza[1, 1] = symbolKomputera;
                return aktualnaPlansza;
            }

            //idac dalej
            if (aktualnaPlansza[0, 0] != symbolKomputera && aktualnaPlansza[0, 0] != null
                && aktualnaPlansza[2, 2] != symbolKomputera && aktualnaPlansza[2, 2] != null
                && aktualnaPlansza[1, 1] == symbolKomputera)
            {
                if (aktualnaPlansza[0, 1] == null)
                {
                    aktualnaPlansza[0, 1] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[2, 0] != symbolKomputera && aktualnaPlansza[2, 0] != null
                && aktualnaPlansza[0, 2] != symbolKomputera && aktualnaPlansza[0, 2] != null
                && aktualnaPlansza[1, 1] == symbolKomputera)
            {
                if (aktualnaPlansza[1, 0] == null)
                {
                    aktualnaPlansza[1, 0] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[1, 1] == symbolKomputera && aktualnaPlansza[1, 0] != symbolKomputera && aktualnaPlansza[1, 0] != null)
            {
                if (aktualnaPlansza[2, 0] == null)
                {
                    aktualnaPlansza[2, 0] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            if (aktualnaPlansza[1, 1] == symbolKomputera && aktualnaPlansza[1, 2] != symbolKomputera && aktualnaPlansza[1, 2] != null)
            {
                if (aktualnaPlansza[2, 2] == null)
                {
                    aktualnaPlansza[2, 2] = symbolKomputera;
                    return aktualnaPlansza;
                }
            }

            //i ostatnie jesli wszystkie powyzsze sa false
            if (aktualnaPlansza[0, 0] == null)
            {
                aktualnaPlansza[0, 0] = symbolKomputera;
                return aktualnaPlansza;
            }

            if (aktualnaPlansza[2, 0] == null)
            {
                aktualnaPlansza[2, 0] = symbolKomputera;
                return aktualnaPlansza;
            }

            if (aktualnaPlansza[2, 2] == null)
            {
                aktualnaPlansza[2, 2] = symbolKomputera;
                return aktualnaPlansza;
            }

            if (aktualnaPlansza[2, 1] == null)
            {
                aktualnaPlansza[2, 1] = symbolKomputera;
                return aktualnaPlansza;
            }

            //poprawka
            if (aktualnaPlansza[1, 0] == null)
            {
                aktualnaPlansza[1, 0] = symbolKomputera;
                return aktualnaPlansza;
            }

            if (aktualnaPlansza[0, 1] == null)
            {
                aktualnaPlansza[0, 1] = symbolKomputera;
                return aktualnaPlansza;
            }

            if (aktualnaPlansza[1, 1] == null)
            {
                aktualnaPlansza[1, 1] = symbolKomputera;
                return aktualnaPlansza;
            }

            if (aktualnaPlansza[1, 2] == null)
            {
                aktualnaPlansza[1, 2] = symbolKomputera;
                return aktualnaPlansza;
            }

            if (aktualnaPlansza[0, 2] == null)
            {
                aktualnaPlansza[0, 2] = symbolKomputera;
                return aktualnaPlansza;
            }
            return null;
        }


        public string CheckWinner(string[,] aktualnaPlansza)
        {
            if (aktualnaPlansza[0,0] == "X")
            {
                if (aktualnaPlansza[0,1] == "X")
                {
                    if (aktualnaPlansza[0,2] == "X")
                    {
                        return "X";
                    }
                }
            }

            if (aktualnaPlansza[1, 0] == "X")
            {
                if (aktualnaPlansza[1, 1] == "X")
                {
                    if (aktualnaPlansza[1, 2] == "X")
                    {
                        return "X";
                    }
                }
            }

            if (aktualnaPlansza[2, 0] == "X")
            {
                if (aktualnaPlansza[2, 1] == "X")
                {
                    if (aktualnaPlansza[2, 2] == "X")
                    {
                        return "X";
                    }
                }
            }

            if (aktualnaPlansza[0, 0] == "X")
            {
                if (aktualnaPlansza[1, 0] == "X")
                {
                    if (aktualnaPlansza[2, 0] == "X")
                    {
                        return "X";
                    }
                }
            }

            if (aktualnaPlansza[0, 1] == "X")
            {
                if (aktualnaPlansza[1, 1] == "X")
                {
                    if (aktualnaPlansza[2, 1] == "X")
                    {
                        return "X";
                    }
                }
            }

            if (aktualnaPlansza[0, 2] == "X")
            {
                if (aktualnaPlansza[1, 2] == "X")
                {
                    if (aktualnaPlansza[2, 2] == "X")
                    {
                        return "X";
                    }
                }
            }

            if (aktualnaPlansza[0, 0] == "X")
            {
                if (aktualnaPlansza[1, 1] == "X")
                {
                    if (aktualnaPlansza[2, 2] == "X")
                    {
                        return "X";
                    }
                }
            }

            if (aktualnaPlansza[0, 2] == "X")
            {
                if (aktualnaPlansza[1, 1] == "X")
                {
                    if (aktualnaPlansza[2, 0] == "X")
                    {
                        return "X";
                    }
                }
            }
            
            //dla o

            if (aktualnaPlansza[0, 0] == "O")
            {
                if (aktualnaPlansza[0, 1] == "O")
                {
                    if (aktualnaPlansza[0, 2] == "O")
                    {
                        return "O";
                    }
                }
            }

            if (aktualnaPlansza[1, 0] == "O")
            {
                if (aktualnaPlansza[1, 1] == "O")
                {
                    if (aktualnaPlansza[1, 2] == "O")
                    {
                        return "O";
                    }
                }
            }

            if (aktualnaPlansza[2, 0] == "O")
            {
                if (aktualnaPlansza[2, 1] == "O")
                {
                    if (aktualnaPlansza[2, 2] == "O")
                    {
                        return "O";
                    }
                }
            }

            if (aktualnaPlansza[0, 0] == "O")
            {
                if (aktualnaPlansza[1, 0] == "O")
                {
                    if (aktualnaPlansza[2, 0] == "O")
                    {
                        return "O";
                    }
                }
            }

            if (aktualnaPlansza[0, 1] == "O")
            {
                if (aktualnaPlansza[1, 1] == "O")
                {
                    if (aktualnaPlansza[2, 1] == "O")
                    {
                        return "O";
                    }
                }
            }

            if (aktualnaPlansza[0, 2] == "O")
            {
                if (aktualnaPlansza[1, 2] == "O")
                {
                    if (aktualnaPlansza[2, 2] == "O")
                    {
                        return "O";
                    }
                }
            }

            if (aktualnaPlansza[0, 0] == "O")
            {
                if (aktualnaPlansza[1, 1] == "O")
                {
                    if (aktualnaPlansza[2, 2] == "O")
                    {
                        return "O";
                    }
                }
            }

            if (aktualnaPlansza[0, 2] == "O")
            {
                if (aktualnaPlansza[1, 1] == "O")
                {
                    if (aktualnaPlansza[2, 0] == "O")
                    {
                        return "O";
                    }
                }
            }

            if (aktualnaPlansza[0,0] != null && aktualnaPlansza[1,0] != null && aktualnaPlansza[2,0] != null && aktualnaPlansza[0,1] != null && aktualnaPlansza[1,1] != null && aktualnaPlansza[2,1] != null && aktualnaPlansza[0,2] != null && aktualnaPlansza[1,2] != null && aktualnaPlansza[2,2] != null )
            {
                return "remis";
            }

            return null;
        }
    }
}
