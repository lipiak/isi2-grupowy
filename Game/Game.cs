﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameContracts;

namespace Game
{
    public class Game : IGame
    {
        string IP;
        string owner;

        public Game(string IP, string owner)
        {
            this.IP = IP;
            this.owner = owner;
        }


        public string GetIP()
        {
            return IP;
        }

        public string GetOwner()
        {
            return owner;
        }
    }
}
