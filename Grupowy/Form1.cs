﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RegistrationContracts;
using Registration;
using Ninject;
using DatabaseContracts;
using Database;
using Connection;
using ConnectionContracts;
using Game;
using GameContracts;
using Logic;
using LogicContracts;
using UserStats;
using UserStatsContracts;


namespace Grupowy
{
    public partial class Form1 : Form
    {
        IKernel ninject;
        ranking RankingForm;
        plansza Plansza;
        GameList gameList;

        public Form1()
        {
            ninject = new StandardKernel();
            ninject.Bind<IRegistration>().To<Registration.Registration>().InSingletonScope();
            ninject.Bind<IDatabase>().To<Database.Database>().InSingletonScope();
            ninject.Bind<IConnection>().To<Connection.Connection>().InSingletonScope();
            ninject.Bind<IGame>().To<Game.Game>().InSingletonScope();
            ninject.Bind<ILogic>().To<Logic.Logic>().InSingletonScope();

            RankingForm = new ranking();
            RankingForm.Hide();
            
            InitializeComponent();
        }

        private void btnZarejestruj_Click(object sender, EventArgs e)
        {
            IRegistration reg = ninject.Get<IRegistration>();
            reg.onRegisterCompleted += this.onRegisterCompleted;
            this.Hide();
            reg.ShowWindow();
        }

        public void onRegisterCompleted(object sender, string msg)
        {
            ((IRegistration)sender).HideWindow();
            this.Show();
        }

        private void btnMultiplayer_Click(object sender, EventArgs e)
        {
            //logowanie tutaj przeprowadzić
            IDatabase db = ninject.Get<IDatabase>();
            if (db.LoginUser(tbLogin.Text, tbHaslo.Text))
            {
                this.Hide();
                gameList = new GameList(this, ninject.Get<IDatabase>(), ninject.Get<IConnection>(), ninject.Get<ILogic>(), tbLogin.Text);
                gameList.Show();
            }
            else
                MessageBox.Show("Niepoprawne dane logowania");

        }

        private void btnStatystyki_Click(object sender, EventArgs e)
        {
            //pobierz staty z bazy
            IDatabase db = ninject.Get<IDatabase>();
            IList<IUserStats> staty = db.GetStatistics();
            RankingForm.WypelnijRanking(staty);
            RankingForm.Show();
        }

        private void btnOffline_Click(object sender, EventArgs e)
        {
            this.Hide();
            Plansza = new plansza(this, ninject.Get<ILogic>());
            Plansza.Show();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (gameList != null)
                gameList.Close();

            if (Plansza != null)
                Plansza.Close();

            if (RankingForm != null)
                RankingForm.Close();

            Application.ExitThread();

            Application.Exit();
        }
    }
}
