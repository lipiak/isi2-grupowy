﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//test
namespace RegistrationContracts
{    
    public delegate void RegistrationEventHandler(object sender, string msg);

    public interface IRegistration
    {
        void ShowWindow();
        void HideWindow();
        event RegistrationEventHandler onRegisterCompleted;
    }
}
