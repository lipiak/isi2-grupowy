﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicContracts
{
    public interface ILogic
    {
        /* przymuje plansze i sprawdza gdzie postawic symbol komputera */
        string[,] PCMove(string[,] aktualnaPlansza, string symbolKomputera);
        
        /* Zwraca symbol, który wygrał. Jeśli zwróci pusty string, to nierozstrzygnięte */
        string CheckWinner(string[,] aktualnaPlansza);
    }
}
