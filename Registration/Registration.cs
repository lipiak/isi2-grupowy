﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RegistrationContracts;
using DatabaseContracts;

namespace Registration
{
    public class Registration : IRegistration
    {
        RegisterForm forma;
        IDatabase db;

        public Registration(IDatabase db)
        {
            forma = new RegisterForm(this);
            this.db = db;
        }

        public void ShowWindow()
        {
            forma.Show();
        }

        public void ZakonczRejestracje()
        {
            onRegisterCompleted.Invoke(this, "register ok");
        }

        public event RegistrationEventHandler onRegisterCompleted;


        public void HideWindow()
        {
            forma.Hide();
        }

        public IDatabase GetDB()
        {
            return db;
        }
    }
}
