﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UserStatsContracts;

namespace Grupowy
{
    public partial class ranking : Form
    {
        public ranking()
        {
            InitializeComponent();
        }

        public void WypelnijRanking(IList<IUserStats> staty)
        {
            //skasuj stare
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            listBox3.Items.Clear();
            listBox4.Items.Clear();
            listBox5.Items.Clear();
            listBox6.Items.Clear();

            //wypelnij
            foreach (IUserStats u in staty)
            {
                listBox1.Items.Add(u.GetNick());
                listBox2.Items.Add(u.GetTotalGames());
                listBox3.Items.Add(u.GetWonGames());
                listBox4.Items.Add(u.GetLostGames());
                listBox5.Items.Add(u.GetWinToLostRatio());
                listBox6.Items.Add(u.GetTieGames());
            }
        }

        private void ranking_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }
    }
}
