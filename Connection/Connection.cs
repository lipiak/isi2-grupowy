﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConnectionContracts;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace Connection
{
    public class Connection : IConnection
    {
        private string user;
        private TcpClient currentPlayer = null;
        private Thread playerThread = null;
        //private Thread serverThread = null;
        private Thread listenThread = null;
        private TcpListener listener = null;
        private NetworkStream client_ns = null;
        private byte[] buffer;
        private int bytesRead;
        private bool on;
        private int serv_port;
        private IPAddress serv_address;

        public Connection()
        {
        }
        /* Uruchamia serwer na komputerze lokalnym, rozpoczyna nasłuch
         * na przychodzące połączenia i odbiór danych. Gdy ktoś się podłączy, wyzwala event
         * onPlayerConnected, gdy dane zostaną odebrane, wyzawalny jest event 
           onMessageReceived */

        public void StartServer(int p)
        {

            on = false;
            serv_port = p;
            //IPAddress adres = IPAddress.Parse("127.0.0.1");
            //serv_address = adres;
            listener = new TcpListener(IPAddress.Any, serv_port);
            this.listener.Start();  
            this.listenThread = new Thread(new ThreadStart(ListenForClient));
            listenThread.Start();

        }


        /* To samo jak wyżej, z tym, że nie czeka na przychodzące połączenie.
         * Jedynie łączy się do kogoś innego i nasłuchuje na przychodzące wiadomośći.
         * Jeśli odbierze wiadomość, wywołuje onMessageReceived*/
        public void ConnectToGame(string ip, string nick)
        {
            user = nick;
            string[] userAddress = new string[2];
            userAddress = ip.Split(':');
            try
            {
                IPAddress ip_ad = IPAddress.Parse(userAddress[0]);
                int port = Convert.ToInt32(userAddress[1]);
                TcpClient player = new TcpClient();
                player.Connect(ip_ad, port);
                currentPlayer = player;
                client_ns = player.GetStream();
                string wiadomosc = string.Format("CONNECTED {0}", nick);
                ASCIIEncoding encoder = new ASCIIEncoding();
                byte[] buffer = encoder.GetBytes(wiadomosc);
                client_ns.Write(buffer, 0, buffer.Length);
                client_ns.Flush();
                playerThread = new Thread(new ThreadStart(HandleClient));
                playerThread.Start();
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /* Wysyła wiadomość przez otwarte połączenie tcp */
        public void Send(string msg)
        {
            NetworkStream clientStream = currentPlayer.GetStream();
            ASCIIEncoding encoder = new ASCIIEncoding();
            byte[] buffer = encoder.GetBytes(msg);
            clientStream.Write(buffer, 0, buffer.Length);
            clientStream.Flush();
        }

        /* Zamyka wszystkie aktualnie działające połączenia */
        public void CloseConnections()
        {
            if (playerThread != null)
                playerThread.Abort();
            
            if(currentPlayer != null)
                currentPlayer.Close();

            if (listener != null)
                listener.Stop();

            if (listenThread != null)
                listenThread.Abort();

            if (client_ns != null)
            {
                client_ns.Close();
                client_ns.Dispose();
            }
        }

        /* Wyzwalany, gdy przyjdzie nowa wiadomość po tcp */
        public event MessageReceivedHandler onMessageReceived;

        /* Wyzwalany, gdy do gry dołączy nowy gracz */
        public event PlayerConnectedHandler onPlayerConnected;

        private void ListenForClient()
        {

            try
            {
                this.currentPlayer = this.listener.AcceptTcpClient();
                on = true;

                //nasluch na wiadomosci gracza
                playerThread = new Thread(new ThreadStart(HandleClient));
                playerThread.Start();
                return;
            }
            catch (Exception e)
            {
                return;
            }
            
            
           //this.onPlayerConnected.Invoke(this, user);

        }
        private void HandleClient()
        {
            NetworkStream clientStream = currentPlayer.GetStream();
            byte[] message = new byte[4096];

            while (true)
            {
                bytesRead = 0;

                try
                {
                    //czeka na wiadomosc
                    bytesRead = clientStream.Read(message, 0, 4096);
                }
                catch
                {
                    break;
                }

                if (bytesRead == 0) break;
                //wiadomosc doszla
                ASCIIEncoding encoder = new ASCIIEncoding();
                string wiadomosc = encoder.GetString(message, 0, bytesRead);


                string[] po_spacjach = wiadomosc.Split(' ');
                if (po_spacjach[0].Equals("CONNECTED"))
                    onPlayerConnected.Invoke(this, po_spacjach[1]);
                else
                    onMessageReceived.Invoke(this, wiadomosc);
            }
        }

    }
}
