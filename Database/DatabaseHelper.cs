﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Database
{
    /// <summary>
    /// tu miało być więcej rzeczy ale okazało się, że na tak mały projekt to w sumie jeden chuj czy tu w czy w klasie Database
    /// </summary>
    internal static class DatabaseHelper
    {

        public static bool OpenConnection(MySqlConnection connection)
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                switch (ex.Number)
                {
                    case 0:
                        MessageBox.Show("nie mozna sie połaczyć z bazą");
                        break;
                    case 1045:
                        MessageBox.Show("bledny login lub haslo");
                        break;
                }
                return false;
            }
        }

        public static bool CloseConnection(MySqlConnection connection)
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

       
    }
}
